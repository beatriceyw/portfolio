package com.example.demo.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileComponent {
	private static final Logger log = LoggerFactory.getLogger(FileComponent.class);

	private final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMddHHmmssSSS");

	// 파일저장
	public Map<String, String> fileUpload(MultipartFile files){
		
		Map<String, String> map = new HashMap<String, String>();
		String path = System.getProperty("user.dir")+"/file/"; 
		System.out.println(path);
		File filePath = new File(path);
		if (filePath.exists() == false) {
			filePath.mkdirs();
		}
		
		String dbPath = "/image/file/"; // 디비 저장 경로
		
		Date date = new Date(System.currentTimeMillis()); // 파일명 앞에 Date 추가

		String originalFilename = SDF.format(date)+"_" + files.getOriginalFilename();
		String fileName = path +  originalFilename;
		String dbfileName = dbPath + originalFilename;

		byte[] bytes;
		try {
			bytes = files.getBytes();
			File file = new File(fileName);
			FileOutputStream out = new FileOutputStream(file);
			out.write(bytes);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		map.put("fileName", dbfileName);

		return map;
	
	}
	
}
