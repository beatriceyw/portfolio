package com.example.demo.config;

import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import lombok.AllArgsConstructor;


@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Override
	public void configure(WebSecurity web) throws Exception {
		// static디렉터리의 하위 파일 목록은 인증 무시 (= 항상통과 )
		web.ignoring().antMatchers("/resources/static/**");
	}
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/admin/login").permitAll()
                .antMatchers("/admin/**").authenticated()
                .anyRequest().permitAll();
        http.headers().frameOptions().disable();
		http.csrf().disable();
        http.formLogin()
            // .loginPage("/admin/login") // 로그인 view 로그인 폼의 아이디,패스워드 username, password로 
            .loginProcessingUrl("/admin/signIn")// 로그인form의  action
            .defaultSuccessUrl("/admin/planning/list"); // 로그인 성공 시 이동할 경로.
            http.logout()
            .permitAll()
            .logoutUrl("/admin/logout")
            .logoutSuccessUrl("/login")
            .and();
    }

    @Bean
    public BCryptPasswordEncoder encodePwd() {
        return new BCryptPasswordEncoder();
    }

    @Bean
	public ServletListenerRegistrationBean<HttpSessionEventPublisher> httpSessionEventPublisher() {
		return new ServletListenerRegistrationBean<HttpSessionEventPublisher>(new HttpSessionEventPublisher());
	}

}