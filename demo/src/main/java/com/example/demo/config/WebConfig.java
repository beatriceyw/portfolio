package com.example.demo.config;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    private static final Logger log = LoggerFactory.getLogger(WebConfig.class);

    // @Value("${file.resources}")
    // private String resources;

    @Value("${file.image}")
    private String imagePath;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
        // .allowedOrigins("http://localhost:3000"); //배포서버 IP 차후 추가
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String path = System.getProperty("user.dir");
           
        File imageFile = new File(path+imagePath);
        // File resourcesFile = new File(path+resources);

        if(!imageFile.exists()){
            imageFile.mkdirs();
        }

        // if(!resourcesFile.exists()){
        //     resourcesFile.mkdirs();
        // }
        
        registry.addResourceHandler("/image/**")
                .addResourceLocations(imageFile.toURI().toString());
        // registry.addResourceHandler("/resources/**")
        //         .addResourceLocations(resourcesFile.toURI().toString());

    }

}