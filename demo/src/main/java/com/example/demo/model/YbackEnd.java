package com.example.demo.model;

import java.time.Instant;
import java.time.LocalDateTime;

import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Field.Write;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mongodb.lang.Nullable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Document(collection = "y_backEnd")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class YbackEnd {
  @Id
  private ObjectId id;
	private String summary;
  private String title;
  private String contents;
  private String thumbnail;
  // @Field(name="insertDate", write = NON_NULL)
  // @CreatedDate
  // private Instant insertDate;
  @LastModifiedDate
  private Instant updateDate;
  // private LocalDateTime updateDate;
  private int openStatus;
  private int hitCnt;
}
