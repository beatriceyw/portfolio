package com.example.demo.model;

import java.time.LocalDateTime;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Document(collection = "y_frontEnd")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class YfrontEnd {
  @Id
  private ObjectId id;
	private String summary;
  private String title;
  private String contents;
  private String thumbnail;
  // @CreatedDate
  // private LocalDateTime insertDate;
  @LastModifiedDate
  private LocalDateTime updateDate;
  private int openStatus;
  private int hitCnt;
}
