package com.example.demo.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Document(collection = "y_file")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Yfile {
    @Id
	private ObjectId id;
	private String filePath;
	private String viewId;
	private String category;
}
