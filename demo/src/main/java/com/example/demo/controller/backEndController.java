package com.example.demo.controller;

import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.BackEndDto;
import com.example.demo.dto.FileDto;
import com.example.demo.service.BackEndService;
import com.example.demo.service.FileService;

import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class backEndController {

	@Autowired
  	private BackEndService BackEndService;
	
	@Autowired
  	private FileService FileService;

    private static final Logger log = LoggerFactory.getLogger(indexController.class);

	@RequestMapping(value = "/backEnd", method = RequestMethod.GET)
	public ModelAndView getBackEndList(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("backEnd/list");
	
		List<BackEndDto> backEndList = BackEndService.getBackEndEndList();

		mv.addObject("backEndList", backEndList);

		return mv;

	}

	@RequestMapping(value = "/backEnd/{Id}", method = RequestMethod.GET)
	public ModelAndView getBackEndView(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("backEnd/view");

		BackEndDto backEnd = null;

		backEnd = BackEndService.getBackEndView(id);

		mv.addObject("backEnd", backEnd);

		//file 리스트 불러오기
		List<FileDto> fileList = FileService.findByViewIdAndCategory(id.toString(), "backEnd");

		if(fileList.size() > 0){
			mv.addObject("fileList", fileList);
		}
		
		return mv;
	}

}

