package com.example.demo.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;
import org.thymeleaf.util.StringUtils;

import com.example.demo.dto.FileDto;
import com.example.demo.dto.PublishingDto;
import com.example.demo.service.FileService;
import com.example.demo.service.PublishingService;
import com.example.demo.util.FileComponent;

@Controller
@RequestMapping(value = "/admin")
public class AdminPublishingController {

	@Autowired
  	private PublishingService PublishingService;

	@Autowired
  	private FileService FileService;

	@Autowired
	private FileComponent file;

	@RequestMapping(value = "/publishing/list", method = RequestMethod.GET)
	public ModelAndView getAdminPublishingList(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();	
		// List<Map<String, Object>> feedList = new ArrayList<>();
	
		List<PublishingDto> publishList = PublishingService.getAdminPublishingList();

		mv.addObject("publishingList", publishList);

		return mv;

	}

	@RequestMapping(value = "/publishing/list/{Id}", method = RequestMethod.GET)
	public ModelAndView getPublishingView(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("admin/publishing/write");

		PublishingDto PublishingDto = null;
		
		PublishingDto = PublishingService.getPublishingView(id);
		
		mv.addObject("Publishing", PublishingDto);
		
		//file 리스트 불러오기
		List<FileDto> fileList = FileService.findByViewIdAndCategory(id.toString(), "publishing");

		if(fileList.size() > 0){
			mv.addObject("fileList", fileList);
		}
		
		return mv;
	}
	@RequestMapping(value = "/publishing/write", method = RequestMethod.GET)
	public ModelAndView publishingWrite(
		HttpServletResponse response
	) {
		ModelAndView mv = new ModelAndView("admin/publishing/write");

		return mv;
  	}
	@RequestMapping(value = "/publishing/delete", method = RequestMethod.GET)
	public ModelAndView deletePublishingView(
	@RequestParam(defaultValue = "0", value="Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();

		if(id != null){
			PublishingService.deletePublishingView(id);
		}
		
		mv.setViewName("redirect:/admin/publishing/list");

		return mv;
	}

	@RequestMapping(value = "/publishing/save", method = RequestMethod.POST)
	public ModelAndView handleAdminPublishing(PublishingDto PublishingDto, HttpServletRequest request,
	MultipartFile[] fileName ) {

		ModelAndView mv = new ModelAndView();

		String summary = PublishingDto.getContents().replaceAll("<([^>]+)>", "");
		String thumbnail = request.getParameter("thumbnail");
		String publishingViewId = "";

		summary = HtmlUtils.htmlUnescape(summary);
		PublishingDto.setSummary(summary);
		
		if(StringUtils.isEmpty(thumbnail)){
			thumbnail = "";
			PublishingDto.setThumbnail(thumbnail);
		}else{
			thumbnail = request.getParameterValues("thumbnail")[0];
			thumbnail = thumbnail.replace(" ", "%20");
			thumbnail = thumbnail.replace("(", "\\(");
			thumbnail = thumbnail.replace(")", "\\)");
			PublishingDto.setThumbnail(thumbnail);
		}

		PublishingService.savePublishingView(PublishingDto);

		if(PublishingDto.getId() == null){
			publishingViewId = PublishingService.getLastpublishingViewId().getId().toString();
		}else{
			publishingViewId = PublishingDto.getId().toString();
		}

		// 첨부 파일 저장
		Map<String, String> map = new HashMap<String, String>();

		if(fileName.length > 0) {
			for(MultipartFile f: fileName){
				if(!f.isEmpty()) {
					map = new HashMap<String, String>();
					map = file.fileUpload(f);
					FileDto Yfile = FileDto.builder()
						.viewId(publishingViewId)
						.filePath(map.get("fileName").toString())
						.category("publishing")
						.build();
					
					FileService.saveFile(Yfile);
				}
			}	
		}

		mv.setViewName("redirect:/admin/publishing/list");
		return mv;
	}

}
