package com.example.demo.controller.admin;

import com.example.demo.dto.FileDto;
import com.example.demo.dto.MongoDbDto;
import com.example.demo.service.FileService;
import com.example.demo.service.MongoDbService;
import com.example.demo.util.FileComponent;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;
import org.thymeleaf.util.StringUtils;

@Controller
@RequestMapping(value = "/admin")
public class AdminMongoDbController {

	@Autowired
  	private MongoDbService MongoDbService;

	@Autowired
  	private FileService FileService;

	@Autowired
	private FileComponent file;

	@RequestMapping(value = "/mongoDB/list", method = RequestMethod.GET)
	public ModelAndView getAdminMongoDbList(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();	
	
		List<MongoDbDto> mongoDbList = MongoDbService.getAdminMongoDbList();

		mv.addObject("mongoDbList", mongoDbList);

		return mv;

	}

	@RequestMapping(value = "/mongoDB/list/{Id}", method = RequestMethod.GET)
	public ModelAndView getMongoDbView(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("admin/mongoDB/write");
		int adminRank = 0; // 0이 관리자 

		MongoDbDto MongoDb = null;
		
		MongoDb = MongoDbService.getMongoDbView(id);
			
		mv.addObject("MongoDb", MongoDb);
		
		//file 리스트 불러오기
		List<FileDto> fileList = FileService.findByViewIdAndCategory(id.toString(), "mongoDb");

		if(fileList.size() > 0){
			mv.addObject("fileList", fileList);
		}

		return mv;
	}

	@RequestMapping(value = "/mongoDB/write", method = RequestMethod.GET)
	public ModelAndView MongoDBWrite(
		HttpServletResponse response
	) {
		ModelAndView mv = new ModelAndView("admin/mongoDB/write");

		return mv;
  	}

	@RequestMapping(value = "/mongoDB/delete", method = RequestMethod.GET)
	public ModelAndView deleteMongoDbView(
	@RequestParam(defaultValue = "0", value="Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();

		if(id != null){
			MongoDbService.deleteMongoDbView(id);
		}
		
		mv.setViewName("redirect:/admin/mongoDB/list");

		return mv;
	}

	@RequestMapping(value = "/mongoDB/save", method = RequestMethod.POST)
	public ModelAndView handleAdminMongoDb(MongoDbDto MongoDbDto, HttpServletRequest request,
	MultipartFile[] fileName ) {

		ModelAndView mv = new ModelAndView();

		String summary = MongoDbDto.getContents().replaceAll("<([^>]+)>", "");
		String thumbnail = request.getParameter("thumbnail");
		String mongoDbViewId = "";

		summary = HtmlUtils.htmlUnescape(summary);
		MongoDbDto.setSummary(summary);
		
		if(StringUtils.isEmpty(thumbnail)){
			thumbnail = "";
			MongoDbDto.setThumbnail(thumbnail);
		}else{
			thumbnail = request.getParameterValues("thumbnail")[0];
			thumbnail = thumbnail.replace(" ", "%20");
			thumbnail = thumbnail.replace("(", "\\(");
			thumbnail = thumbnail.replace(")", "\\)");
			MongoDbDto.setThumbnail(thumbnail);
		}

		MongoDbService.saveMongoDbView(MongoDbDto);

		if(MongoDbDto.getId() == null){
			mongoDbViewId = MongoDbService.getLastMongoDbViewId().getId().toString();
		}else{
			mongoDbViewId = MongoDbDto.getId().toString();
		}

		// 첨부 파일 저장
		Map<String, String> map = new HashMap<String, String>();

		if(fileName.length > 0) {
			for(MultipartFile f: fileName){
				if(!f.isEmpty()) {
					map = new HashMap<String, String>();
					map = file.fileUpload(f);
					FileDto Yfile = FileDto.builder()
						.viewId(mongoDbViewId)
						.filePath(map.get("fileName").toString())
						.category("mongoDB")
						.build();
					
					FileService.saveFile(Yfile);
				}
			}	
		}

		mv.setViewName("redirect:/admin/mongoDB/list");
		return mv;
	}
}
