package com.example.demo.controller;

import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.MariaDbDto;
import com.example.demo.service.MariaDbService;

import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class mariaDbController {

	@Autowired
  	private MariaDbService MariaDbService;
	
    private static final Logger log = LoggerFactory.getLogger(indexController.class);

	@RequestMapping(value = "/mariaDB", method = RequestMethod.GET)
	public ModelAndView mariaDB(HttpServletRequest request){
		ModelAndView mv  = new ModelAndView("mariaDB/list");

		List<MariaDbDto> mariaDBList = MariaDbService.getMariaDbList();

		mv.addObject("mariaDBList", mariaDBList);

		return mv;
	}
	
	@RequestMapping(value = "/mariaDB/{Id}", method = RequestMethod.GET)
	public ModelAndView getMariaDbView(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("mariaDB/view");

		MariaDbDto mariaDB = null;
		
		mariaDB = MariaDbService.getMariaDbView(id);

		mv.addObject("mariaDB", mariaDB);

		return mv;
	}

}	

