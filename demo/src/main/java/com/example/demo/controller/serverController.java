package com.example.demo.controller;

import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.ServerDto;
import com.example.demo.service.ServerService;

import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class serverController {

	@Autowired
  	private ServerService ServerService;
	
    private static final Logger log = LoggerFactory.getLogger(indexController.class);

	@RequestMapping(value = "/server", method = RequestMethod.GET)
	public ModelAndView getAdminServerList(HttpServletRequest request, @RequestParam(defaultValue = "0", value = "page") int page, @RequestParam(defaultValue = "20", value = "size") int size,
	@RequestParam(defaultValue = "", value = "kwd") String keyword, @RequestParam(defaultValue = "", value = "jwt") String jwt) {
		ModelAndView mv = new ModelAndView("server/list");	
	
		List<ServerDto> serverList = ServerService.getServerList();

		mv.addObject("serverList", serverList);

		return mv;

	}

	@RequestMapping(value = "/server/{Id}", method = RequestMethod.GET)
	public ModelAndView getServerView(
	@PathVariable("Id") ObjectId id) {
		ModelAndView mv = new ModelAndView("server/view");

		ServerDto server = null;
		
		server = ServerService.getServerView(id);
		
		mv.addObject("server", server);
			
		return mv;
	}

}

