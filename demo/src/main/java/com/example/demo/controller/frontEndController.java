package com.example.demo.controller;

import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.FrontEndDto;
import com.example.demo.service.FrontEndService;

import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class frontEndController {

	@Autowired
  	private FrontEndService FrontEndService;

    private static final Logger log = LoggerFactory.getLogger(frontEndController.class);

	@RequestMapping(value = "/frontEnd", method = RequestMethod.GET)
	public ModelAndView frontEnd(HttpServletRequest request){
		ModelAndView mv  = new ModelAndView("frontEnd/list");

		List<FrontEndDto> frontEnd = FrontEndService.getFrontEndList();
		mv.addObject("FrontEndList", frontEnd);

		return mv;
	}
	
	@RequestMapping(value = "/frontEnd/{Id}", method = RequestMethod.GET)
	public ModelAndView getfrontEndView(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("frontEnd/view");

		FrontEndDto frontEnd = null;

		frontEnd = FrontEndService.getFrontEndView(id);

		mv.addObject("FrontEnd", frontEnd);

		return mv;
	}

}

