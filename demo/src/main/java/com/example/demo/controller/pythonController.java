package com.example.demo.controller;

import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.PythonDto;
import com.example.demo.service.PythonService;

import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class pythonController {

	@Autowired
  	private PythonService PythonService;
	
    private static final Logger log = LoggerFactory.getLogger(indexController.class);

	@RequestMapping(value = "/python", method = RequestMethod.GET)
	public ModelAndView getPythonList(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("python/list");
	
		List<PythonDto> pythonList = PythonService.getPythonList();

		mv.addObject("pythonList", pythonList);

		return mv;

	}

	@RequestMapping(value = "/python/{Id}", method = RequestMethod.GET)
	public ModelAndView getPythonView(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("python/view");

		PythonDto python = null;

		python = PythonService.getPythonView(id);

		mv.addObject("python", python);

		return mv;
	}

}

