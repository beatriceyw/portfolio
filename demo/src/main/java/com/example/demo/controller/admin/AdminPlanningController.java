package com.example.demo.controller.admin;

import com.example.demo.dto.FileDto;
import com.example.demo.dto.PlanningDto;
import com.example.demo.service.FileService;
import com.example.demo.service.planningService;
import com.example.demo.util.FileComponent;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;
import org.thymeleaf.util.StringUtils;

@Controller
@RequestMapping(value = "/admin")
public class AdminPlanningController {

	@Autowired
  	private planningService planningService;

	@Autowired
  	private FileService FileService;

	@Autowired
	private FileComponent file;


	@RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
	public ModelAndView redirectDefaultPage(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("redirect:/admin/backEnd/list");
		
		return mv;
	}

	@RequestMapping(value = "/planning/list", method = RequestMethod.GET)
	public ModelAndView getAdminPlanningList(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();	
		// List<Map<String, Object>> feedList = new ArrayList<>();
	
		List<PlanningDto> planningList = planningService.getAdminPlanningList();

		mv.addObject("planningList", planningList);

		return mv;

	}

	@RequestMapping(value = "/planning/list/{Id}", method = RequestMethod.GET)
	public ModelAndView getPlanningView(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("admin/planning/write");

		PlanningDto planning = null;
		planning = planningService.getPlanningView(id);
		
		mv.addObject("planning", planning);
		
		//file 리스트 불러오기
		List<FileDto> fileList = FileService.findByViewIdAndCategory(id.toString(), "planning");
		
		if(fileList.size() > 0){
			mv.addObject("fileList", fileList);
		}

		return mv;
	}

	@RequestMapping(value = "/planning/write", method = RequestMethod.GET)
	public ModelAndView planningWrite(
		HttpServletResponse response
	) {
		ModelAndView mv = new ModelAndView("admin/planning/write");

		return mv;
  	}

	@RequestMapping(value = "/planning/delete", method = RequestMethod.GET)
	public ModelAndView deletePlanningView(
	@RequestParam(defaultValue = "0", value="Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();

		if(id != null){
			planningService.deletePlanningView(id);
		}
		
		mv.setViewName("redirect:/admin/planning/list");

		return mv;
	}

	@RequestMapping(value = "/planning/save", method = RequestMethod.POST)
	public ModelAndView handleAdminPlanning(PlanningDto PlanningDto, HttpServletRequest request,
	MultipartFile[] fileName ) {

		ModelAndView mv = new ModelAndView();

		String summary = PlanningDto.getContents().replaceAll("<([^>]+)>", "");
		String thumbnail = request.getParameter("thumbnail");
		String planningViewId = "";

		summary = HtmlUtils.htmlUnescape(summary);
		PlanningDto.setSummary(summary);
		
		if(StringUtils.isEmpty(thumbnail)){
			thumbnail = "";
			PlanningDto.setThumbnail(thumbnail);
		}else{
			thumbnail = request.getParameterValues("thumbnail")[0];
			thumbnail = thumbnail.replace(" ", "%20");
			thumbnail = thumbnail.replace("(", "\\(");
			thumbnail = thumbnail.replace(")", "\\)");
			PlanningDto.setThumbnail(thumbnail);
		}

		planningService.savePlanningView(PlanningDto);

		if(PlanningDto.getId() == null){
			planningViewId = planningService.getLastplanningViewId().getId().toString();
		}else{
			planningViewId = PlanningDto.getId().toString();
		}

		// 첨부 파일 저장
		Map<String, String> map = new HashMap<String, String>();

		if(fileName.length > 0) {
			for(MultipartFile f: fileName){
				if(!f.isEmpty()) {
					map = new HashMap<String, String>();
					map = file.fileUpload(f);
					FileDto Yfile = FileDto.builder()
						.viewId(planningViewId)
						.filePath(map.get("fileName").toString())
						.category("planning")
						.build();
					
					FileService.saveFile(Yfile);
				}
			}	
		}

		mv.setViewName("redirect:/admin/planning/list");
		return mv;
	}
}
