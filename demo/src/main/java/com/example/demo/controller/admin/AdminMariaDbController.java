package com.example.demo.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;
import org.thymeleaf.util.StringUtils;

import com.example.demo.dto.BackEndDto;
import com.example.demo.dto.FileDto;
import com.example.demo.dto.MariaDbDto;
import com.example.demo.service.BackEndService;
import com.example.demo.service.FileService;
import com.example.demo.service.MariaDbService;
import com.example.demo.util.FileComponent;

@Controller
@RequestMapping(value = "/admin")
public class AdminMariaDbController {

	@Autowired
  	private MariaDbService MariaDbService;

	@Autowired
  	private FileService FileService;

	@Autowired
	private FileComponent file;

	@RequestMapping(value = "/mariaDB/list", method = RequestMethod.GET)
	public ModelAndView getMariaDBList(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();	
	
		List<MariaDbDto> mariaDbList = MariaDbService.getAdminMariaDbList();

		mv.addObject("mariaDbList", mariaDbList);

		return mv;

	}
	@RequestMapping(value = "/mariaDB/list/{Id}", method = RequestMethod.GET)
	public ModelAndView getAdminMariaDbView(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("admin/mariaDB/write");

		MariaDbDto mariaDB = null;
		
		mariaDB = MariaDbService.getMariaDbView(id);

		mv.addObject("mariaDB", mariaDB);

		//file 리스트 불러오기
		List<FileDto> fileList = FileService.findByViewIdAndCategory(id.toString(), "mariaDB");

		if(fileList.size() > 0){
			mv.addObject("fileList", fileList);
		}

		return mv;
	}
	@RequestMapping(value = "/mariaDB/write", method = RequestMethod.GET)
	public ModelAndView mariaDbWrite(
		HttpServletResponse response
	) {
		ModelAndView mv = new ModelAndView("admin/mariaDB/write");

		return mv;
  	}

	@RequestMapping(value = "/mariaDB/delete", method = RequestMethod.GET)
	public ModelAndView deleteMariaDbView(
	@RequestParam(defaultValue = "0", value="Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();

		if(id != null){
			MariaDbService.deleteMariaDbView(id);
		}
		
		mv.setViewName("redirect:/admin/mariaDB/list");

		return mv;
			
	}
	@RequestMapping(value = "/mariaDB/save", method = RequestMethod.POST)
	public ModelAndView handleAdminMariaDB(MariaDbDto MariaDbDto, HttpServletRequest request,
	MultipartFile[] fileName ) {

		ModelAndView mv = new ModelAndView();

		String summary = MariaDbDto.getContents().replaceAll("<([^>]+)>", "");
		String thumbnail = request.getParameter("thumbnail");
		String mariaDbViewId = "";

		summary = HtmlUtils.htmlUnescape(summary);
		MariaDbDto.setSummary(summary);
		
		if(StringUtils.isEmpty(thumbnail)){
			thumbnail = "";
			MariaDbDto.setThumbnail(thumbnail);
		}else{
			thumbnail = request.getParameterValues("thumbnail")[0];
			thumbnail = thumbnail.replace(" ", "%20");
			thumbnail = thumbnail.replace("(", "\\(");
			thumbnail = thumbnail.replace(")", "\\)");
			MariaDbDto.setThumbnail(thumbnail);
		}

		MariaDbService.saveMariaDbView(MariaDbDto);

		if(MariaDbDto.getId() == null){
			mariaDbViewId = MariaDbService.getLastMariaDbViewId().getId().toString();
		}else{
			mariaDbViewId = MariaDbDto.getId().toString();
		}

		// 첨부 파일 저장
		Map<String, String> map = new HashMap<String, String>();

		if(fileName.length > 0) {
			for(MultipartFile f: fileName){
				if(!f.isEmpty()) {
					map = new HashMap<String, String>();
					map = file.fileUpload(f);
					FileDto Yfile = FileDto.builder()
						.viewId(mariaDbViewId)
						.filePath(map.get("fileName").toString())
						.category("mariaDB")
						.build();
					
					FileService.saveFile(Yfile);
				}
			}	
		}

		mv.setViewName("redirect:/admin/mariaDB/list");
		return mv;
	}

}
