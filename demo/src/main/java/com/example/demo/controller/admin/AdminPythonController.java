package com.example.demo.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;
import org.thymeleaf.util.StringUtils;

import com.example.demo.dto.FileDto;
import com.example.demo.dto.PythonDto;
import com.example.demo.service.FileService;
import com.example.demo.service.PythonService;
import com.example.demo.util.FileComponent;

@Controller
@RequestMapping(value = "/admin")
public class AdminPythonController {

	@Autowired
  	private PythonService PythonService;

	@Autowired
  	private FileService FileService;

	@Autowired
	private FileComponent file;

	@RequestMapping(value = "/python/list", method = RequestMethod.GET)
	public ModelAndView getAdminPythonList(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();	
	
		List<PythonDto> pythonList = PythonService.getAdminPythonList();

		mv.addObject("pythonList", pythonList);

		return mv;

	}

	@RequestMapping(value = "/python/list/{Id}", method = RequestMethod.GET)
	public ModelAndView getPython(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("admin/python/write");

		PythonDto python = null;

		python = PythonService.getPythonView(id);

		mv.addObject("python", python);

		//file 리스트 불러오기
		List<FileDto> fileList = FileService.findByViewIdAndCategory(id.toString(), "python");

		if(fileList.size() > 0){
			mv.addObject("fileList", fileList);
		}

		return mv;
	}

	@RequestMapping(value = "/python/write", method = RequestMethod.GET)
	public ModelAndView pythonWrite(
		HttpServletResponse response
	) {
		ModelAndView mv = new ModelAndView("admin/python/write");

		return mv;
  	}

	@RequestMapping(value = "/python/delete", method = RequestMethod.GET)
	public ModelAndView deleteBackEnd(
	@RequestParam(defaultValue = "0", value="Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();

		if(id != null){
			PythonService.deletePythonView(id);
		}
		
		mv.setViewName("redirect:/admin/python/list");

		return mv;
			
	}

	@RequestMapping(value = "/python/save", method = RequestMethod.POST)
	public ModelAndView handleAdminPython(PythonDto PythonDto, HttpServletRequest request,
	MultipartFile[] fileName ) {

		ModelAndView mv = new ModelAndView();

		String summary = PythonDto.getContents().replaceAll("<([^>]+)>", "");
		String thumbnail = request.getParameter("thumbnail");
		String pythonViewId = "";

		summary = HtmlUtils.htmlUnescape(summary);
		PythonDto.setSummary(summary);
		
		if(StringUtils.isEmpty(thumbnail)){
			thumbnail = "";
			PythonDto.setThumbnail(thumbnail);
		}else{
			thumbnail = request.getParameterValues("thumbnail")[0];
			thumbnail = thumbnail.replace(" ", "%20");
			thumbnail = thumbnail.replace("(", "\\(");
			thumbnail = thumbnail.replace(")", "\\)");
			PythonDto.setThumbnail(thumbnail);
		}
		PythonService.savePythonView(PythonDto);

		if(PythonDto.getId() == null){
			pythonViewId = PythonService.getLastPythonViewId().getId().toString();
		}else{
			pythonViewId = PythonDto.getId().toString();
		}

		// 첨부 파일 저장
		Map<String, String> map = new HashMap<String, String>();

		if(fileName.length > 0) {
			for(MultipartFile f: fileName){
				if(!f.isEmpty()) {
					map = new HashMap<String, String>();
					map = file.fileUpload(f);
					FileDto Yfile = FileDto.builder()
						.viewId(pythonViewId)
						.filePath(map.get("fileName").toString())
						.category("backEnd")
						.build();
					
					FileService.saveFile(Yfile);
				}
			}	
		}

		mv.setViewName("redirect:/admin/python/list");
		return mv;
	}
}
