package com.example.demo.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;
import org.thymeleaf.util.StringUtils;

import com.example.demo.dto.BackEndDto;
import com.example.demo.dto.FileDto;
import com.example.demo.service.BackEndService;
import com.example.demo.service.FileService;
import com.example.demo.util.FileComponent;

@Controller
@RequestMapping(value = "/admin")
public class AdminBackEndController {

	@Autowired
  	private BackEndService BackEndService;

	@Autowired
  	private FileService FileService;

	@Autowired
	private FileComponent file;

	@RequestMapping(value = "/backEnd/list", method = RequestMethod.GET)
	public ModelAndView getAdminBackEndList(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();	
	
		List<BackEndDto> backEndList = BackEndService.getAdminBackEndList();

		mv.addObject("backEndList", backEndList);

		return mv;

	}

	@RequestMapping(value = "/backEnd/list/{Id}", method = RequestMethod.GET)
	public ModelAndView getBackEnd(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("admin/backEnd/write");

		BackEndDto backEnd = null;

		backEnd = BackEndService.getBackEndView(id);

		mv.addObject("backEnd", backEnd);

		//file 리스트 불러오기
		List<FileDto> fileList = FileService.findByViewIdAndCategory(id.toString(), "backEnd");
		
		if(fileList.size() > 0){
			mv.addObject("fileList", fileList);
		}

		return mv;
	}

	@RequestMapping(value = "/backEnd/write", method = RequestMethod.GET)
	public ModelAndView BackEndWrite(
		HttpServletResponse response
	) {
		ModelAndView mv = new ModelAndView("admin/backEnd/write");

		return mv;
  	}

	@RequestMapping(value = "/backEnd/delete", method = RequestMethod.GET)
	public ModelAndView deleteBackEnd(
	@RequestParam(defaultValue = "0", value="Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();

		if(id != null){
			BackEndService.deleteBackEndView(id);
		}
		
		mv.setViewName("redirect:/admin/backEnd/list");

		return mv;
			
	}

	@RequestMapping(value = "/backEnd/save", method = RequestMethod.POST)
	public ModelAndView handleAdminBackEnd(BackEndDto BackEndDto, HttpServletRequest request,
	MultipartFile[] fileName ) {

		ModelAndView mv = new ModelAndView();

		String summary = BackEndDto.getContents().replaceAll("<([^>]+)>", "");
		String thumbnail = request.getParameter("thumbnail");
		String backEndViewId = "";

		summary = HtmlUtils.htmlUnescape(summary);
		BackEndDto.setSummary(summary);
		
		if(StringUtils.isEmpty(thumbnail)){
			thumbnail = "";
			BackEndDto.setThumbnail(thumbnail);
		}else{
			thumbnail = request.getParameterValues("thumbnail")[0];
			thumbnail = thumbnail.replace(" ", "%20");
			thumbnail = thumbnail.replace("(", "\\(");
			thumbnail = thumbnail.replace(")", "\\)");
			BackEndDto.setThumbnail(thumbnail);
		}
		BackEndService.saveBackEndView(BackEndDto);

		if(BackEndDto.getId() == null){
			backEndViewId = BackEndService.getLastBackEndViewId().getId().toString();
		}else{
			backEndViewId = BackEndDto.getId().toString();
		}

		// 첨부 파일 저장
		Map<String, String> map = new HashMap<String, String>();

		if(fileName.length > 0) {
			for(MultipartFile f: fileName){
				if(!f.isEmpty()) {
					map = new HashMap<String, String>();
					map = file.fileUpload(f);
					FileDto Yfile = FileDto.builder()
						.viewId(backEndViewId)
						.filePath(map.get("fileName").toString())
						.category("backEnd")
						.build();

					FileService.saveFile(Yfile);
				}
			}	
		}

		mv.setViewName("redirect:/admin/backEnd/list");
		return mv;
	}
}
