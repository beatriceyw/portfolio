package com.example.demo.controller.admin;

import com.example.demo.dto.FileDto;
import com.example.demo.dto.ServerDto;
import com.example.demo.service.FileService;
import com.example.demo.service.ServerService;
import com.example.demo.util.FileComponent;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;
import org.thymeleaf.util.StringUtils;

@Controller
@RequestMapping(value = "/admin")
public class AdminServerController {

	@Autowired
  	private ServerService ServerService;

	@Autowired
  	private FileService FileService;

	@Autowired
	private FileComponent file;

	@RequestMapping(value = "/server/list", method = RequestMethod.GET)
	public ModelAndView getAdminServerList(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();	
	
		List<ServerDto> serverList = ServerService.getAdminServerList();

		mv.addObject("serverList", serverList);

		return mv;

	}
	@RequestMapping(value = "/server/list/{Id}", method = RequestMethod.GET)
	public ModelAndView getServerView(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("admin/server/write");

		ServerDto server = null;
		
		server = ServerService.getServerView(id);
		
		mv.addObject("server", server);
		
		//file 리스트 불러오기
		List<FileDto> fileList = FileService.findByViewIdAndCategory(id.toString(), "publishing");

		if(fileList.size() > 0){
			mv.addObject("fileList", fileList);
		}

		return mv;
	}
	@RequestMapping(value = "/server/write", method = RequestMethod.GET)
	public ModelAndView serverWrite(
		HttpServletResponse response
	) {
		ModelAndView mv = new ModelAndView("admin/server/write");

		return mv;
  	}
	@RequestMapping(value = "/server/delete", method = RequestMethod.GET)
	public ModelAndView deleteServerView(
	@RequestParam(defaultValue = "0", value="Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();

		if(id != null){
			ServerService.deleteServerView(id);
		}
		
		mv.setViewName("redirect:/admin/server/list");

		return mv;
	}

	@RequestMapping(value = "/server/save", method = RequestMethod.POST)
	public ModelAndView handleAdminBackEnd(ServerDto ServerDto, HttpServletRequest request,
	MultipartFile[] fileName ) {

		ModelAndView mv = new ModelAndView();

		String summary = ServerDto.getContents().replaceAll("<([^>]+)>", "");
		String thumbnail = request.getParameter("thumbnail");
		String serverViewId = "";

		summary = HtmlUtils.htmlUnescape(summary);
		ServerDto.setSummary(summary);
		
		if(StringUtils.isEmpty(thumbnail)){
			thumbnail = "";
			ServerDto.setThumbnail(thumbnail);
		}else{
			thumbnail = request.getParameterValues("thumbnail")[0];
			thumbnail = thumbnail.replace(" ", "%20");
			thumbnail = thumbnail.replace("(", "\\(");
			thumbnail = thumbnail.replace(")", "\\)");
			ServerDto.setThumbnail(thumbnail);
		}

		ServerService.saveServerView(ServerDto);

		if(ServerDto.getId() == null){
			serverViewId = ServerService.getLastServerViewId().getId().toString();
		}else{
			serverViewId = ServerDto.getId().toString();
		}

		// 첨부 파일 저장
		Map<String, String> map = new HashMap<String, String>();

		if(fileName.length > 0) {
			for(MultipartFile f: fileName){
				if(!f.isEmpty()) {
					map = new HashMap<String, String>();
					map = file.fileUpload(f);
					FileDto Yfile = FileDto.builder()
						.viewId(serverViewId)
						.filePath(map.get("fileName").toString())
						.category("server")
						.build();
					
					FileService.saveFile(Yfile);
				}
			}	
		}

		mv.setViewName("redirect:/admin/server/list");
		return mv;
	}

}
