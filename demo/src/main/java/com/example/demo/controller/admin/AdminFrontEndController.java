package com.example.demo.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.thymeleaf.util.StringUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import com.example.demo.dto.FileDto;
import com.example.demo.dto.FrontEndDto;
import com.example.demo.service.FileService;
import com.example.demo.service.FrontEndService;
import com.example.demo.util.FileComponent;

@Controller
@RequestMapping(value = "/admin")
public class AdminFrontEndController {

	@Autowired
  	private FrontEndService FrontEndService;

	@Autowired
  	private FileService FileService;

	@Autowired
	private FileComponent file;

	@RequestMapping(value = "/frontEnd/list", method = RequestMethod.GET)
	public ModelAndView getAdminFrontEndList(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();	
	
		List<FrontEndDto> FrontEndList = FrontEndService.getAdminFrontEndList();

		mv.addObject("FrontEndList", FrontEndList);

		return mv;

	}
	@RequestMapping(value = "/frontEnd/list/{Id}", method = RequestMethod.GET)
	public ModelAndView getFrontEndView(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("admin/frontEnd/write");

		FrontEndDto frontEnd = null;

		frontEnd = FrontEndService.getFrontEndView(id);

		mv.addObject("frontEnd", frontEnd);

		//file 리스트 불러오기
		List<FileDto> fileList = FileService.findByViewIdAndCategory(id.toString(), "frontEnd");

		if(fileList.size() > 0){
			mv.addObject("fileList", fileList);
		}

		return mv;
	}
	@RequestMapping(value = "/frontEnd/write", method = RequestMethod.GET)
	public ModelAndView FrontEndWrite(
		// @RequestParam(defaultValue = "0", value = "Id") ObjectId id,
		HttpServletResponse response
	) {
		ModelAndView mv = new ModelAndView("admin/frontEnd/write");

		return mv;
  	}
	@RequestMapping(value = "/frontEnd/delete", method = RequestMethod.GET)
	public ModelAndView deleteFrontEndView(
	@RequestParam(defaultValue = "0", value="Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();

		if(id != null){
			FrontEndService.deleteFrontEndView(id);
		}
		
		mv.setViewName("redirect:/admin/frontEnd/list");

		return mv;
			
	}
	@RequestMapping(value = "/frontEnd/save", method = RequestMethod.POST)
	public ModelAndView handleAdminFrontEnd(FrontEndDto FrontEndDto, HttpServletRequest request,
	MultipartFile[] fileName ) {
	// private long handleAdminBackEnd(BackEndDto BackEnd ,MultipartFile[] fileName, String [] tagName, HttpServletRequest request) {

		ModelAndView mv = new ModelAndView();

		String summary = FrontEndDto.getContents().replaceAll("<([^>]+)>", "");
		String thumbnail = request.getParameter("thumbnail");
		String FrontEndViewId = "";
		
		summary = HtmlUtils.htmlUnescape(summary);
		FrontEndDto.setSummary(summary);
		
		if(StringUtils.isEmpty(thumbnail)){
			thumbnail = "";
			FrontEndDto.setThumbnail(thumbnail);
		}else{
			thumbnail = request.getParameterValues("thumbnail")[0];
			thumbnail = thumbnail.replace(" ", "%20");
			thumbnail = thumbnail.replace("(", "\\(");
			thumbnail = thumbnail.replace(")", "\\)");
			FrontEndDto.setThumbnail(thumbnail);
		}

		FrontEndService.saveFrontEndView(FrontEndDto);
		if(FrontEndDto.getId() == null){
			FrontEndViewId = FrontEndService.getLastFrontEndViewId().getId().toString();
		}else{
			FrontEndViewId = FrontEndDto.getId().toString();
		}
		// 첨부 파일 저장
		Map<String, String> map = new HashMap<String, String>();

		if(fileName.length > 0) {
			for(MultipartFile f: fileName){
				if(!f.isEmpty()) {
					map = new HashMap<String, String>();
					map = file.fileUpload(f);
					FileDto Yfile = FileDto.builder()
						.viewId(FrontEndViewId)
						.filePath(map.get("fileName").toString())
						.category("FrontEnd")
						.build();
					
					FileService.saveFile(Yfile);
				}
			}	
		}

		mv.setViewName("redirect:/admin/frontEnd/list");
		return mv;
	}
}
