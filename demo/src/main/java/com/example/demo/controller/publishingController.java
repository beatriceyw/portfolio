package com.example.demo.controller;

import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.PublishingDto;
import com.example.demo.service.PublishingService;

import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class publishingController {

	@Autowired
  	private PublishingService PublishingService;
	
    private static final Logger log = LoggerFactory.getLogger(indexController.class);

	@RequestMapping(value = "/publishing", method = RequestMethod.GET)
	public ModelAndView getPublishingList(HttpServletRequest request, @RequestParam(defaultValue = "0", value = "page") int page, @RequestParam(defaultValue = "20", value = "size") int size,
	@RequestParam(defaultValue = "", value = "kwd") String keyword, @RequestParam(defaultValue = "", value = "jwt") String jwt) {
		ModelAndView mv = new ModelAndView("publishing/list");	
	
		List<PublishingDto> publishList = PublishingService.getPublishingList();

		mv.addObject("publishingList", publishList);

		return mv;

	}

	@RequestMapping(value = "/publishing/{Id}", method = RequestMethod.GET)
	public ModelAndView getPublishingView(@ModelAttribute("session") HttpSession session,
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("publishing/view");

		PublishingDto PublishingDto = null;
		
		PublishingDto = PublishingService.getPublishingView(id);
		
		mv.addObject("Publishing", PublishingDto);
			
		return mv;
	}

}

