package com.example.demo.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.swing.Spring;

import org.apache.commons.io.FilenameUtils;
import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.service.FileService;


@Controller
public class CommonController {
    
	@Autowired
  	private FileService FileService;
	
    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

	@Value("${file.image}")
	private String imagePath;

	private final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		
    // 에디터 이미지 업로드
	@RequestMapping(value = "/uploadImageFile", produces = "application/json; charset=utf8")
	@ResponseBody
	public String uploadSummernoteImageFile(@RequestParam("file") MultipartFile files, 
	@RequestParam(defaultValue ="", value = "path") String path, HttpServletRequest request) {
		JSONObject jsonObject = new JSONObject();
		
		String SystemPath = System.getProperty("user.dir");   
        File imageFile = new File(SystemPath+imagePath+"/"+path+"/");
		String filePath = imageFile.getPath()+"/";// 저장될 폴더경로
		if (imageFile.exists() == false) {
			imageFile.mkdirs();
		}

		String dbPath = "/image/"+path+"/"; // 디비 저장 경로
		
		Date date = new Date(System.currentTimeMillis()); // 파일명 앞에 Date 추가
		// 년월일시분초 14자리 포멧
		
		// 파일저장
		String fileName = filePath + SDF.format(date)+"."+FilenameUtils.getExtension(files.getOriginalFilename());
		String dbfileName = dbPath + SDF.format(date)+"."+FilenameUtils.getExtension(files.getOriginalFilename());
		byte[] bytes;
		try {
			bytes = files.getBytes();
			File file = new File(fileName);
			FileOutputStream out = new FileOutputStream(file);
			out.write(bytes);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// List<String> cmd = new ArrayList<String>();
		// cmd.add("python3");
		// cmd.add("/var/api/rd_img1.py");
		// cmd.add("-f");
		// cmd.add(fileName);
		// ProcessBuilder builder = new ProcessBuilder(cmd);
		// try {
		// 	Process krx = builder.start();
		// 	BufferedReader stdOut = new BufferedReader( new InputStreamReader(krx.getInputStream()) );
		// 	String str ="";
		// 	while( ( str = stdOut.readLine()) != null ) {
		// 		log.info("str {}" ,str);
		// 	}
		// } catch (IOException e) {
		// 	e.printStackTrace();
		// }

		jsonObject.put( "url", dbfileName);
		String a = jsonObject.toString();

		return a;
	}

	@RequestMapping(value = "/fileDel", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object>deleteFile(@RequestBody Map<String, Object> reqmap){
		Map<String, Object> map  = new HashMap<String, Object>();
		System.out.println(reqmap.get("id"));
		String fileId = String.valueOf(reqmap.get("id"));
		// ObjectId fileId = new ObjectId(reqmap.get("id"));

		if(fileId != null) {
			// TODO delete real file
			FileService.deleteFile(fileId);
			map.put("result", 1);
		}else {
			map.put("result", 0);
		}	

		return map;
	}

}