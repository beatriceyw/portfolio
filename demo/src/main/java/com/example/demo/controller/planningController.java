package com.example.demo.controller;

import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.PlanningDto;
import com.example.demo.service.planningService;

import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class planningController {

	@Autowired
  	private planningService planningService;
	
    private static final Logger log = LoggerFactory.getLogger(indexController.class);


	@RequestMapping(value = "/planning", method = RequestMethod.GET)
	public ModelAndView getPlanningList(HttpServletRequest request, @RequestParam(defaultValue = "0", value = "page") int page, @RequestParam(defaultValue = "20", value = "size") int size,
	@RequestParam(defaultValue = "", value = "kwd") String keyword, @RequestParam(defaultValue = "", value = "jwt") String jwt) {
		ModelAndView mv = new ModelAndView("planning/list");	
	
		List<PlanningDto> planningList = planningService.getPlanningList();

		mv.addObject("planningList", planningList);

		return mv;

	}

	@RequestMapping(value = "/planning/{Id}", method = RequestMethod.GET)
	public ModelAndView getPlanningView(
	@PathVariable("Id") ObjectId id, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("planning/view");

		PlanningDto planning = null;
		planning = planningService.getPlanningView(id);
		
		mv.addObject("planning", planning);
			
		return mv;
	}

}

