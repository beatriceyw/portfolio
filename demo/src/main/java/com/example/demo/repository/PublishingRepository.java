package com.example.demo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Ypublishing;

public interface PublishingRepository extends MongoRepository<Ypublishing, ObjectId>{
    //admin
    List<Ypublishing> findAll();
    @Aggregation(pipeline = {
        "{'$project' : {'_id': 1}}", 
        "{'$sort' : {'_id': -1}}",
        "{'$limit' : 1})"
    })
    List<Ypublishing> getLastpublishingViewId();
    //front
    List<Ypublishing> findTop8ByOrderByIdDesc();
    List<Ypublishing> findOneById(ObjectId id);
}
