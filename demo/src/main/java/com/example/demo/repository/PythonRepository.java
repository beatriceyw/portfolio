package com.example.demo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.dto.PythonDto;
import com.example.demo.model.Ypython;

public interface PythonRepository extends MongoRepository<Ypython, ObjectId>{
    //admin
    List<Ypython> findAll();
    @Aggregation(pipeline = {
        "{'$project' : {'_id': 1}}", 
        "{'$sort' : {'_id': -1}}",
        "{'$limit' : 1})"
    })
    List<Ypython> getLastPythonViewId();
    //front
    List<Ypython> findTop8ByOrderByIdDesc();
    List<Ypython> findOneById(ObjectId id);

}
