package com.example.demo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.Yfile;

public interface FileRepository extends MongoRepository<Yfile, ObjectId>{
    public void deleteById(String id);
    List<Yfile> findByViewIdAndCategory(String id, String category);
}
