package com.example.demo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Ydevelop;

public interface FeedRepository extends MongoRepository<Ydevelop, ObjectId>{

    List<Ydevelop> findOneById(ObjectId id);

}
