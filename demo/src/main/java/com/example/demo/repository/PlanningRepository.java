package com.example.demo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Yplanning;

public interface PlanningRepository extends MongoRepository<Yplanning, ObjectId>{
    //admin
    List<Yplanning> findAll();
    @Aggregation(pipeline = {
        "{'$project' : {'_id': 1}}", 
        "{'$sort' : {'_id': -1}}",
        "{'$limit' : 1})"
    })
    List<Yplanning> getLastplanningViewId();
    //front
    List<Yplanning> findTop8ByOrderByIdDesc();
    List<Yplanning> findOneById(ObjectId id);
}
