package com.example.demo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.YmariaDB;
import com.example.demo.model.YmongoDB;

public interface MariaDbRepository extends MongoRepository<YmariaDB, ObjectId>{
    //admin
    List<YmariaDB> findAll();
    @Aggregation(pipeline = {
        "{'$project' : {'_id': 1}}", 
        "{'$sort' : {'_id': -1}}",
        "{'$limit' : 1})"
    })
    List<YmariaDB> getLastMariaDbViewId();
    //front
    List<YmariaDB> findTop8ByOrderByIdDesc();
    List<YmariaDB> findOneById(ObjectId id);
}
