package com.example.demo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.model.YfrontEnd;

@Repository
public interface FrontEndRepository extends MongoRepository<YfrontEnd, ObjectId>{
    //admin
    List<YfrontEnd> findAll();
    @Aggregation(pipeline = {
        "{'$project' : {'_id': 1}}", 
        "{'$sort' : {'_id': -1}}",
        "{'$limit' : 1})"
    })
    List<YfrontEnd> getLastFrontEndViewId();
    //front
    List<YfrontEnd> findTop8ByOrderByIdDesc();
    List<YfrontEnd> findOneById(ObjectId id);
}
