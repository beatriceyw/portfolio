package com.example.demo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.dto.BackEndDto;
import com.example.demo.model.YbackEnd;

public interface BackEndRepository extends MongoRepository<YbackEnd, ObjectId>{
    //admin
    List<YbackEnd> findAll();
    @Aggregation(pipeline = {
        "{'$project' : {'_id': 1}}", 
        "{'$sort' : {'_id': -1}}",
        "{'$limit' : 1})"
    })
    List<YbackEnd> getLastBackEndViewId();
    //front
    List<YbackEnd> findTop8ByOrderByIdDesc();
    List<YbackEnd> findOneById(ObjectId id);

}
