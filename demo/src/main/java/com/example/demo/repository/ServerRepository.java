package com.example.demo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Yserver;

public interface ServerRepository extends MongoRepository<Yserver, ObjectId>{
    //admin
    List<Yserver> findAll();
    @Aggregation(pipeline = {
        "{'$project' : {'_id': 1}}", 
        "{'$sort' : {'_id': -1}}",
        "{'$limit' : 1})"
    })
    List<Yserver> getLastServerViewId();
    //front
    List<Yserver> findTop8ByOrderByIdDesc();
    List<Yserver> findOneById(ObjectId id);
}
