package com.example.demo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.YmongoDB;

public interface MongoDbRepository extends MongoRepository<YmongoDB, ObjectId> {
    //admin
    List<YmongoDB> findAll();
    @Aggregation(pipeline = {
        "{'$project' : {'_id': 1}}", 
        "{'$sort' : {'_id': -1}}",
        "{'$limit' : 1})"
    })
    List<YmongoDB> getLastMongoDbViewId();
    //front
    List<YmongoDB> findTop8ByOrderByIdDesc();
    List<YmongoDB> findOneById(ObjectId id);
}
