package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.ServerDto;
import com.example.demo.model.Yserver;
import com.example.demo.repository.FrontEndRepository;
import com.example.demo.repository.ServerRepository;
@Service
public class ServerService {

    @Autowired
	private ServerRepository ServerRepository;

	@Autowired
	private ModelMapper modelMapper;

	public List<ServerDto> getAdminServerList() {
		List<ServerDto> adminServerListDto = new ArrayList<>();
		List<Yserver> resultList = ServerRepository.findAll();

	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                adminServerListDto.add(modelMapper.map(data, ServerDto.class));
            });
		}

		return adminServerListDto;
	}

    public List<ServerDto> getServerList() {
		List<ServerDto> ServerListDto = new ArrayList<>();
		List<Yserver> resultList = ServerRepository.findAll();

	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                ServerListDto.add(modelMapper.map(data, ServerDto.class));
            });
		}

		return ServerListDto;
	}
	public ServerDto getServerView(ObjectId id) {
		ServerDto ServerDto = null;
		List<Yserver> resultList = ServerRepository.findOneById(id);

	
		if(!resultList.isEmpty()){
			ServerDto = modelMapper.map(resultList.get(0), ServerDto.class);
		}

		return ServerDto;
	}
	public ServerDto getLastServerViewId() {
		ServerDto ServerDto = null;
		List<Yserver> resultList = ServerRepository.getLastServerViewId();

		if(!resultList.isEmpty()){
			ServerDto = modelMapper.map(resultList.get(0), ServerDto.class);
		}
		
		return ServerDto;
	}
	public void saveServerView(ServerDto ServerDto) { 
		Yserver server = modelMapper.map(ServerDto, Yserver.class);
		ServerRepository.save(server); 
	}
	public void deleteServerView(ObjectId id) {
		ServerRepository.deleteById(id);
	}
}
