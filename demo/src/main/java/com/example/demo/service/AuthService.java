package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.dto.AuthDto;
import com.example.demo.model.Yauth;
import com.example.demo.repository.AuthRepository;

@Service
public class AuthService implements UserDetailsService {

    @Autowired
	private AuthRepository AuthRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AuthDto admin_info = null;
		List<Yauth> resultList = AuthRepository.findOneByUsername(username);

		if(resultList.isEmpty()) {
			System.out.println("## 계정정보가 존재하지 않습니다. ##");
			throw new UsernameNotFoundException(username);
		}else{
			admin_info = modelMapper.map(resultList.get(0), AuthDto.class);
		}
		
		return admin_info;

	}

}
