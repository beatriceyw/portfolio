package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.PublishingDto;
import com.example.demo.model.Ypublishing;
import com.example.demo.repository.PublishingRepository;


@Service
public class PublishingService {

    @Autowired
	private PublishingRepository publishingRepository;

	@Autowired
	private ModelMapper modelMapper;

	public List<PublishingDto> getAdminPublishingList() {
		List<PublishingDto> adminPublishingListDto = new ArrayList<>();
		List<Ypublishing> resultList = publishingRepository.findAll();

	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                adminPublishingListDto.add(modelMapper.map(data, PublishingDto.class));
            });
		}

		return adminPublishingListDto;
	}

    public List<PublishingDto> getPublishingList() {
		List<PublishingDto> publishingListDto = new ArrayList<>();
		List<Ypublishing> resultList = publishingRepository.findTop8ByOrderByIdDesc();

	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                publishingListDto.add(modelMapper.map(data, PublishingDto.class));
            });
		}

		return publishingListDto;
	}
	public PublishingDto getPublishingView(ObjectId id) {
		PublishingDto publishingDto = null;
		List<Ypublishing> resultList = publishingRepository.findOneById(id);
	
		if(!resultList.isEmpty()){
			publishingDto = modelMapper.map(resultList.get(0), PublishingDto.class);
		}

		return publishingDto;
	}
	public PublishingDto getLastpublishingViewId() {
		PublishingDto publishingDto = null;
		List<Ypublishing> resultList = publishingRepository.getLastpublishingViewId();

		if(!resultList.isEmpty()){
			publishingDto = modelMapper.map(resultList.get(0), PublishingDto.class);
		}
		
		return publishingDto;
	}
	public void savePublishingView(PublishingDto PublishingDto) { 
		Ypublishing planning = modelMapper.map(PublishingDto, Ypublishing.class);
		publishingRepository.save(planning); 
	}
	public void deletePublishingView(ObjectId id) {
		publishingRepository.deleteById(id);
	}
}
