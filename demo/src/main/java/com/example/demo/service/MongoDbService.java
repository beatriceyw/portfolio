package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.MongoDbDto;
import com.example.demo.model.YbackEnd;
import com.example.demo.model.YmongoDB;
import com.example.demo.repository.MongoDbRepository;

@Service
public class MongoDbService {

    @Autowired
	private MongoDbRepository MongoDbRepository;

	@Autowired
	private ModelMapper modelMapper;

	public List<MongoDbDto> getAdminMongoDbList() {
		List<MongoDbDto> adminMongoDbListDto = new ArrayList<>();
		List<YmongoDB> resultList = MongoDbRepository.findAll();

	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                adminMongoDbListDto.add(modelMapper.map(data, MongoDbDto.class));
            });
		}

		return adminMongoDbListDto;
	}

    public List<MongoDbDto> getMongoDbList() {
		List<MongoDbDto> mongoDbListDto = new ArrayList<>();
		List<YmongoDB> resultList = MongoDbRepository.findTop8ByOrderByIdDesc();

	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                mongoDbListDto.add(modelMapper.map(data, MongoDbDto.class));
            });
		}

		return mongoDbListDto;
	}

	public MongoDbDto getMongoDbView(ObjectId id) {
		MongoDbDto mongoDbDto = null;
		List<YmongoDB> resultList = MongoDbRepository.findOneById(id);

	
		if(!resultList.isEmpty()){
			mongoDbDto = modelMapper.map(resultList.get(0), MongoDbDto.class);
		}

		return mongoDbDto;
	}
	public MongoDbDto getLastMongoDbViewId() {
		MongoDbDto MongoDbDto = null;
		List<YmongoDB> resultList = MongoDbRepository.getLastMongoDbViewId();

		if(!resultList.isEmpty()){
			MongoDbDto = modelMapper.map(resultList.get(0), MongoDbDto.class);
		}
		
		return MongoDbDto;
	}
	public void saveMongoDbView(MongoDbDto MongoDbDto) { 
		YmongoDB MariaDB = modelMapper.map(MongoDbDto, YmongoDB.class);
		MongoDbRepository.save(MariaDB); 
	}

	public void deleteMongoDbView(ObjectId id) {
		MongoDbRepository.deleteById(id);
	}
}
