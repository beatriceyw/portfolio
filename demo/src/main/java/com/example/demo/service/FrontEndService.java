package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.FrontEndDto;
import com.example.demo.model.YfrontEnd;
import com.example.demo.repository.FrontEndRepository;

@Service
public class FrontEndService {

    @Autowired
	private FrontEndRepository FrontEndRepository;

	@Autowired
	private ModelMapper modelMapper;

    public List<FrontEndDto> getAdminFrontEndList() {
		List<FrontEndDto> adminFrontEndList = new ArrayList<>();
		List<YfrontEnd> resultList = FrontEndRepository.findAll();

	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                adminFrontEndList.add(modelMapper.map(data, FrontEndDto.class));
            });
		}

		return adminFrontEndList;
	}

	public List<FrontEndDto> getFrontEndList() {
		List<FrontEndDto> FrontEndDtoList = new ArrayList<>();
		List<YfrontEnd> resultList = FrontEndRepository.findTop8ByOrderByIdDesc();

	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                FrontEndDtoList.add(modelMapper.map(data, FrontEndDto.class));
            });
		}

		return FrontEndDtoList;
	}

	public FrontEndDto getFrontEndView(ObjectId id) {
		FrontEndDto FrontEndDto = null;
		List<YfrontEnd> resultList = FrontEndRepository.findOneById(id);

		if(!resultList.isEmpty()){
			FrontEndDto = modelMapper.map(resultList.get(0), FrontEndDto.class);
		}

		return FrontEndDto;
	}
	public FrontEndDto getLastFrontEndViewId() {
		FrontEndDto FrontEndDto = null;
		List<YfrontEnd> resultList = FrontEndRepository.getLastFrontEndViewId();

		if(!resultList.isEmpty()){
			FrontEndDto = modelMapper.map(resultList.get(0), FrontEndDto.class);
		}
		
		return FrontEndDto;
	}
	public void saveFrontEndView(FrontEndDto FrontEndDto) { 
		YfrontEnd BackEnd = modelMapper.map(FrontEndDto, YfrontEnd.class);
		FrontEndRepository.save(BackEnd); 
	}

	public void deleteFrontEndView(ObjectId id) {
		FrontEndRepository.deleteById(id);
	}
}
