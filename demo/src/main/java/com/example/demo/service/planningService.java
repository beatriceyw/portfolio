package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.PlanningDto;
import com.example.demo.model.Yplanning;
import com.example.demo.repository.PlanningRepository;


@Service
public class planningService {

    @Autowired
	private PlanningRepository planningRepository;

	@Autowired
	private ModelMapper modelMapper;

	public List<PlanningDto> getAdminPlanningList() {
		List<PlanningDto> adminPlanningListDto = new ArrayList<>();
		List<Yplanning> resultList = planningRepository.findAll();

	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                adminPlanningListDto.add(modelMapper.map(data, PlanningDto.class));
            });
		}

		return adminPlanningListDto;
	}

    public List<PlanningDto> getPlanningList() {
		List<PlanningDto> planningDtoListDto = new ArrayList<>();
		List<Yplanning> resultList = planningRepository.findTop8ByOrderByIdDesc();

	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                planningDtoListDto.add(modelMapper.map(data, PlanningDto.class));
            });
		}

		return planningDtoListDto;
	}

	public PlanningDto getPlanningView(ObjectId id) {
		PlanningDto planningDto = null;
		List<Yplanning> resultList = planningRepository.findOneById(id);

	
		if(!resultList.isEmpty()){
			planningDto = modelMapper.map(resultList.get(0), PlanningDto.class);
		}

		return planningDto;
	}
	public PlanningDto getLastplanningViewId() {
		PlanningDto planningDto = null;
		List<Yplanning> resultList = planningRepository.getLastplanningViewId();

		if(!resultList.isEmpty()){
			planningDto = modelMapper.map(resultList.get(0), PlanningDto.class);
		}
		
		return planningDto;
	}
	public void savePlanningView(PlanningDto PlanningDto) { 
		Yplanning planning = modelMapper.map(PlanningDto, Yplanning.class);
		planningRepository.save(planning); 
	}

	public void deletePlanningView(ObjectId id) {
		planningRepository.deleteById(id);
	}
}
