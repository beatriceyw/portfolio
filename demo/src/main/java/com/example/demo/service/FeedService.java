package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.FeedDto;
import com.example.demo.model.Ydevelop;
import com.example.demo.repository.FeedRepository;

@Service
public class FeedService {
	@Autowired
	private FeedRepository FeedRepository;

	@Autowired
	private ModelMapper modelMapper;

	public List<Ydevelop> getFeedList() {
		List<Ydevelop> feedList = FeedRepository.findAll();
		
		return feedList;
	}

	public FeedDto getFeed(ObjectId id) {
		FeedDto feedDto =  null;
		List<Ydevelop> resultList = FeedRepository.findOneById(id);

		if(!resultList.isEmpty()){
			feedDto = modelMapper.map(resultList.get(0), FeedDto.class);
		}

		System.out.println(feedDto);
		return feedDto;
	}

	// public List<FeedDto> getFeed(ObjectId id) {
	// 	List<FeedDto> feedDto =  new ArrayList<>();
	// 	List<Object[]> resultList = FeedRepository.findOneById(id);
	// 	if(!resultList.isEmpty()){
	// 		resultList.forEach(data->{
	// 			feedDto.add(new FeedDto(data));
	// 		});
	// 	}
	// 	return feedDto;
	// }

// 	public List<Eadmin> getAdminUserid(String id) {
// 		List<Eadmin> adminList = adminRepository.findByadminUserid(id);		
// 		return adminList;
// 	}
// 	public List<Eadmin> adminlist() {
// 		List<Eadmin> adminList = adminRepository.findAll();
		
// 		return adminList;
// 	}
// 	public Eadmin getAdminID(long adminId) {
// 		Eadmin eadmin= adminRepository.getOne(adminId);	
// 		return eadmin;
// 	}
// 	public void adminSave(Eadmin eadmin) {
// 		adminRepository.save(eadmin);
// 	}

}
