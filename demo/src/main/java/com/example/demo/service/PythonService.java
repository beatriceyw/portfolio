package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.MongoDbDto;
import com.example.demo.dto.PythonDto;
import com.example.demo.model.Ypython;
import com.example.demo.repository.MongoDbRepository;
import com.example.demo.repository.PythonRepository;

@Service
public class PythonService {

    @Autowired
	private PythonRepository PythonRepository;

	@Autowired
	private ModelMapper modelMapper;

	public List<PythonDto> getAdminPythonList() {
		List<PythonDto> adminPythonListDto = new ArrayList<>();
		List<Ypython> resultList = PythonRepository.findAll();

		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                adminPythonListDto.add(modelMapper.map(data, PythonDto.class));
            });
		}

		return adminPythonListDto;
	}

    public List<PythonDto> getPythonList() {
		List<PythonDto> pythonListDto = new ArrayList<>();
		List<Ypython> resultList = PythonRepository.findTop8ByOrderByIdDesc();

		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                pythonListDto.add(modelMapper.map(data, PythonDto.class));
            });
		}

		return pythonListDto;
	}

	public PythonDto getPythonView(ObjectId id) {
		PythonDto pythonDto = null;
		List<Ypython> resultList = PythonRepository.findOneById(id);

	
		if(!resultList.isEmpty()){
			pythonDto = modelMapper.map(resultList.get(0), PythonDto.class);
		}

		return pythonDto;
	}
	public PythonDto getLastPythonViewId() {
		PythonDto pythonDto = null;
		List<Ypython> resultList = PythonRepository.getLastPythonViewId();

		if(!resultList.isEmpty()){
			pythonDto = modelMapper.map(resultList.get(0), PythonDto.class);
		}
		
		return pythonDto;
	}
	public void savePythonView(PythonDto PythonDto) { 
		Ypython python = modelMapper.map(PythonDto, Ypython.class);
		PythonRepository.save(python); 
	}

	public void deletePythonView(ObjectId id) {
		PythonRepository.deleteById(id);
	}
}
