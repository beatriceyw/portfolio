package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.dto.FileDto;
import com.example.demo.model.Yfile;
import com.example.demo.repository.FileRepository;

import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileService {
    
    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private ModelMapper modelMapper;

    public void saveFile(FileDto fileDto){

        Yfile efile = modelMapper.map(fileDto, Yfile.class);
        fileRepository.save(efile);
    }
    public List<FileDto> findByViewIdAndCategory(String id, String category){

        List<FileDto> fileDtoList = new ArrayList<>();
        List<Yfile> YfileList = fileRepository.findByViewIdAndCategory(id, category);

        if(!YfileList.isEmpty()){
            fileDtoList = modelMapper.map(YfileList,new TypeToken<List<FileDto>>() {}.getType());
        }

        return fileDtoList;
    }
    public void deleteFile(String id){
        fileRepository.deleteById(id);
    }
}
