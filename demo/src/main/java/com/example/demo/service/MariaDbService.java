package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.MariaDbDto;
import com.example.demo.model.YmariaDB;
import com.example.demo.repository.MariaDbRepository;


@Service
public class MariaDbService {

    @Autowired
	private MariaDbRepository MariaDbRepository;

	@Autowired
	private ModelMapper modelMapper;

	public List<MariaDbDto> getAdminMariaDbList() {
		List<MariaDbDto> adminFrontEndListDto = new ArrayList<>();
		List<YmariaDB> resultList = MariaDbRepository.findAll();

	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                adminFrontEndListDto.add(modelMapper.map(data, MariaDbDto.class));
            });
		}

		return adminFrontEndListDto;
	}

    public List<MariaDbDto> getMariaDbList() {
		List<MariaDbDto> MariaDbListDto = new ArrayList<>();
		List<YmariaDB> resultList = MariaDbRepository.findTop8ByOrderByIdDesc();

	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                MariaDbListDto.add(modelMapper.map(data, MariaDbDto.class));
            });
		}

		return MariaDbListDto;
	}

	public MariaDbDto getMariaDbView(ObjectId id) {
		MariaDbDto MariaDbDto = null;
		List<YmariaDB> resultList = MariaDbRepository.findOneById(id);

	
		if(!resultList.isEmpty()){
			MariaDbDto = modelMapper.map(resultList.get(0), MariaDbDto.class);
		}

		return MariaDbDto;
	}
	public MariaDbDto getLastMariaDbViewId() {
		MariaDbDto MariaDbDto = null;
		List<YmariaDB> resultList = MariaDbRepository.getLastMariaDbViewId();

		if(!resultList.isEmpty()){
			MariaDbDto = modelMapper.map(resultList.get(0), MariaDbDto.class);
		}
		
		return MariaDbDto;
	}
	public void saveMariaDbView(MariaDbDto MariaDbDto) { 
		YmariaDB MariaDB = modelMapper.map(MariaDbDto, YmariaDB.class);
		MariaDbRepository.save(MariaDB); 
	}

	public void deleteMariaDbView(ObjectId id) {
		MariaDbRepository.deleteById(id);
	}
}
