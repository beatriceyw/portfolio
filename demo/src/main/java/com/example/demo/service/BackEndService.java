package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.BackEndDto;
import com.example.demo.dto.FrontEndDto;
import com.example.demo.model.YbackEnd;
import com.example.demo.repository.BackEndRepository;

@Service
public class BackEndService {

    @Autowired
	private BackEndRepository BackEndRepository;

	@Autowired
	private ModelMapper modelMapper;

    public List<BackEndDto> getAdminBackEndList() {
		List<BackEndDto> adminBackEndListDto = new ArrayList<>();
		List<YbackEnd> resultList = BackEndRepository.findAll();
	
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                adminBackEndListDto.add(modelMapper.map(data, BackEndDto.class));
            });
		}

		return adminBackEndListDto;
	}

	public List<BackEndDto> getBackEndEndList() {
		List<BackEndDto> BackEndListDto = new ArrayList<>();
		List<YbackEnd> resultList = BackEndRepository.findTop8ByOrderByIdDesc();
		if(!resultList.isEmpty()){
			resultList.forEach(data -> {
                BackEndListDto.add(modelMapper.map(data, BackEndDto.class));
            });
		}
		return BackEndListDto;
	}

	public BackEndDto getBackEndView(ObjectId id) {
		BackEndDto BackEndDto = null;
		List<YbackEnd> resultList = BackEndRepository.findOneById(id);

	
		if(!resultList.isEmpty()){
			BackEndDto = modelMapper.map(resultList.get(0), BackEndDto.class);
		}

		return BackEndDto;
	}
	public BackEndDto getLastBackEndViewId() {
		BackEndDto BackEndDto = null;
		List<YbackEnd> resultList = BackEndRepository.getLastBackEndViewId();

		if(!resultList.isEmpty()){
			BackEndDto = modelMapper.map(resultList.get(0), BackEndDto.class);
		}
		
		return BackEndDto;
	}
	public void saveBackEndView(BackEndDto BackEndDto) { 
		YbackEnd BackEnd = modelMapper.map(BackEndDto, YbackEnd.class);
		BackEndRepository.save(BackEnd); 
	}

	public void deleteBackEndView(ObjectId id) {
		BackEndRepository.deleteById(id);
	}
}
