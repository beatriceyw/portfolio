package com.example.demo.dto;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PublishingDto {
	private ObjectId id;
	private String summary;
	private String title;
	private String contents;
	private String thumbnail;
	private int openStatus;
	private int hitCnt;
	// private String insertDate;
	private String updateDate;
}
