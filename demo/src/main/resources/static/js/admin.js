$(function() {
    // $(document).find('.input-wrap .file input[type=file]').on('change', function () {
    //     console.log('file uploaded');
    //     var fileName = $(this).val();
    //     $(this).parent().find('input[type=text]').val(fileName);
    // });

    $('#save').click(function() {
        const formSave = $("#formSave");
        let thumbnail = $(".note-editable").find('img').eq(0).attr("src");    
    
        if (thumbnail) {
          formSave.append("<input type=hidden name=thumbnail id=thumbnail value='"+thumbnail+"'>");
        }
        
        let inputFile = $("input[name=fileName]");
        const match = /[\{\}\[\]\/?,;:|\)*~`!^\-+<>@\#$%&\\\=\(\'\"]/gi;
        
        for(i=0;i < inputFile.length ;i++ ){
          let file = inputFile.eq(i).val();
          file =file.split('\\').pop().split('/').pop();
          if(match.test(file)){
            alert("첨부파일명에는 특수문자가 들어갈 수 없습니다.");
            return;
          }
        }
      $("#formSave").submit();
    });

    $('#contents').summernote(setting);

});
let toolbar = [
    // 글꼴 설정
    ['fontname', ['fontname']],
    // 글자 크기 설정
    ['fontsize', ['fontsize']],
    // 굵기, 기울임꼴, 밑줄,취소 선, 서식지우기
    ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
    // 글자색
    ['color', ['color']],
    // 표만들기
    ['table', ['table']],
    // 글머리 기호, 번호매기기, 문단정렬
    ['para', ['ul', 'ol', 'paragraph']],
    // 줄간격
    ['height', ['height']],
    // 그림첨부, 링크만들기, 동영상첨부
    ['insert',['picture','link','video']],
    // 코드보기, 확대해서보기, 도움말
    ['view', ['codeview','fullscreen', 'help']]
];
var setting = {
    height : 300,
    minHeight : null,
    maxHeight : null,
    dialogsInBody: true,
    // 에디터에 커서 이동
    focus : true,
    lang : 'ko-KR',
    toolbar : toolbar,
    fontSizes : ['8','9','10','11','12','14','16','18','20','22','24','28','30','36','50','72'],
    fontNames : ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','맑은 고딕','궁서','굴림체','굴림','돋음체','바탕체'],
    //콜백 함수
    callbacks : { 
        onImageUpload : function(files, editor, welEditable) {
        // 파일 업로드(다중업로드를 위해 반복문 사용)
        for (var i = files.length - 1; i >= 0; i--) {
            uploadSummernoteImageFile(files[i], this, window.location.pathname.split("/")[2]);
        }
    }
}
};
//파일찾기 버튼 자동 onclick
function fileOnchange(fileId,fileTextId) {
    console.log('file uploaded');
    var fileName = document.getElementById(fileId).value;
    document.getElementById(fileTextId).value = fileName;
}
//파일 업로드
function fileOnclickBtn(fileId) {
    console.log('file find');
    document.getElementById(fileId).click()
}
//파일 삭제
function fileDel(file) {
    var fileId = file.val();
    if(fileId){
        if(confirm("첨부파일을 삭제하시겠습니까?")){
            $.ajax({
                async : true,
                type : 'POST',
                data : JSON.stringify({"id": fileId}),
                url : "/fileDel",
                dataType : "json",
                contentType : "application/json; charset=UTF-8",
                success : function(data) {	
                    if(data.result == 1){
                        file[0].previousElementSibling.value = ""
                    }				
                },
                error : function(error) {
                    console.log(error);
                }
            });
        }
    }else{
        confirm("등록된 첨부파일이 없습니다. 첨부파일을 업로드해주세요.")
    }
}
function uploadSummernoteImageFile(file, el, path) {
    data = new FormData();
    data.append("file", file );
    data.append("path", path );
    $.ajax({
        data : data,
        type : "POST",
        url : "/uploadImageFile",
        contentType : false,
        enctype : 'multipart/form-data',
        processData : false,
        success : function(data) {
            $(el).summernote('editor.insertImage', data.url);
        }
    });
}
